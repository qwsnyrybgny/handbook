---
title: "GitLab with Git Essentials - Hands-On Lab Overview"
description: "This Hands-On Guide walks you through the lab exercises used in the GitLab with Git Essentials course."
---

## GitLab with Git Essentials Lab Guides

| Lab Name |  Lab Link |
|-----------|------------|
| Create a project and an issue | [Lab Link](/handbook/customer-success/professional-services-engineering/education-services/gitbasicshandsonlab1/) |
| Work With Git Locally |  [Lab Link](/handbook/customer-success/professional-services-engineering/education-services/gitbasicshandsonlab2/) |
| Use GitLab To Merge Code | [Lab Link](/handbook/customer-success/professional-services-engineering/education-services/gitbasicshandsonlab3/) |
| Build a `.gitlab-ci.yml` File | [Lab Link](/handbook/customer-success/professional-services-engineering/education-services/gitbasicshandsonlab4/) |
| Auto DevOps With a Predefined Project Template | [Lab Link](/handbook/customer-success/professional-services-engineering/education-services/gitbasicshandsonlab5/) |
| Static Application Security Testing (SAST) | [Lab Link](/handbook/customer-success/professional-services-engineering/education-services/gitbasicshandsonlab6/) |

## Quick links

* [GitLab with Git Essentials course description](https://about.gitlab.com/services/education/gitlab-basics/)
* [GitLab Certified Associate Certification Details](https://about.gitlab.com/services/education/gitlab-certified-associate/)

## Suggestions?

If you’d like to suggest changes to the *GitLab with Git Essentials Hands-on Guide*, please submit them via merge request.
